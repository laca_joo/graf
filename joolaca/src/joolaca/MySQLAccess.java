/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joolaca;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
/**
 *
 * @author gepem
 */
public class MySQLAccess {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    private String originalTable = "testdb";
    
    
    
    
    public void makeStatement() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/joolaca?"
                            + "user=joolaca&password=joolaca123");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
        } 
        catch (Exception e) {
            throw e;
        } 
    }
    
    public void createOutTable() throws Exception {
        try {                       
            DatabaseMetaData metadata = connect.getMetaData();
            ResultSet resultSet = metadata.getTables(null, null, "outtable", null);            
           
            String str = "CREATE TABLE outtable(" +
                     "id bigint NOT NULL AUTO_INCREMENT," +
                    " point_start bigint,\n" +
                    "point_end bigint,\n" +
                    "CONSTRAINT testdb_pkey PRIMARY KEY (id)\n" +
                    ")";
            if(!resultSet.next()){
                                           
               statement.executeUpdate(str);            
           }    else{
                String drop = "DROP TABLE outtable ;";
                statement.executeUpdate(drop);
                
                statement.executeUpdate(str);
            }
                       

        } catch (Exception e) {
            throw e;
        } 

    }
    
    public void createRoadTable() throws Exception {
        try {                       
            DatabaseMetaData metadata = connect.getMetaData();
            ResultSet resultSet = metadata.getTables(null, null, "road", null);            
           String str = "CREATE TABLE IF NOT EXISTS road AS SELECT * FROM  "+originalTable+";" ;                            
            if(!resultSet.next()){               
               statement.executeUpdate(str);            
           }else{
                String drop = "DROP TABLE road ;";
                statement.executeUpdate(drop);
                
                statement.executeUpdate(str);
            }        
                       

        } catch (Exception e) {
            throw e;
        } 

    }
            
    
    // You need to close the resultSet
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

}
