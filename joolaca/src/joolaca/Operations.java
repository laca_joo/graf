/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joolaca;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author gepem
 */
public class Operations {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    private String originalTable = "testdb";
    
    public void makeStatement() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost/joolaca?"
                            + "user=joolaca&password=joolaca123");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
        } 
        catch (Exception e) {
            throw e;
        } 
    }
    
    
    public boolean scan() throws Exception {
        
        try {
            //System.out.println("***scan()***");                   
                  
            String sql = "";
            sql = "SELECT * from road Limit 1 ";
            //sql = "SELECT * from road WHERE id=4 Limit 1 ";
            ResultSet row = query(sql);
            
            if (row.next()) {  
                
                String point_end = row.getString("point_end");
                String point_start = row.getString("point_start");
                String id = row.getString("id");
                System.out.println("the point " +point_start+ " --- "+ point_end);
                
                
                String inset_start_point = goLine(point_start,id);                
                String inset_end_point = goLine(point_end,id);  
                
                //System.out.println("inset_start_point: " + inset_start_point);
                //System.out.println("inset_end_point: " + inset_end_point);
                
                String insert_str="INSERT INTO outtable(point_start,point_end) "
                        + "VALUES ("+inset_start_point+","+inset_end_point+");";
                
                statement.executeUpdate(insert_str);                
                
                deleteID(id);
                
                scan();
                
            }
        } catch (Exception e) {
            throw e;
        }
        return true;
    }
    
    
    /**
     * Ha a megadott pont point_end annak a pontnak a point_start ját adja vissza
     * és fordítva, közben takarít
     * @param point
     * @param id
     * @return
     * @throws Exception 
     */
    public String goLine(String point, String id) throws Exception{
        if(imNode(point)){ return point; }
        String sql = "SELECT * from "+originalTable+" WHERE point_start ="+point+ " and id !="+id;
        ResultSet rs = query(sql);
        //System.out.println("goLine point: " + point);
        
        if(rs.next()){
            String new_point = rs.getString("point_end");
            String new_id = rs.getString("id");            
            deleteID(new_id);           
            
            return goLine(new_point, new_id);
        }
         sql = "SELECT * from "+originalTable+" WHERE point_end ="+point+ " and id !="+id;
         rs = query(sql);
         
        
        if(rs.next()){
            String new_point = rs.getString("point_start");
            String new_id = rs.getString("id");            
            deleteID(new_id);                                 
            
            return goLine(new_point, new_id);
        }
        
        deleteID(id);
        return point;
    }
    
    /**
     * Megmondja egy pontról hogy csomópont e
     * @param point
     * @return
     * @throws Exception 
     */
    public boolean imNode(String point) throws Exception {
    String check_sql = "SELECT count(id) as count from "+originalTable+" " +
                "WHERE (point_start = "+point+" OR point_end = "+point+")"
            + "Limit 3" ;            
            //System.out.println("checkNode------- " + check_sql);
            ResultSet check_rs = query(check_sql);
            if(check_rs.next()){
                Integer count = (Integer)check_rs.getInt("count");    
                
                if (count > 2) {
                    //System.out.println("imNode------- " + check_sql);
                    return true;
                }else{
                    return false;
                }
            }
            return false;
    }
    
    public void deleteID(String id) throws Exception {
        try {  
            //System.out.println("deleted_id: " + id);
            String delete_str="Delete From road WHERE id="+id;            
           statement.executeUpdate(delete_str);
            
        } catch (Exception e) {
            throw e;
        }
    }
    
    public ResultSet query(String sql) throws Exception {
        try {
                ResultSet row = statement.executeQuery(sql);
                return row;
        } catch (Exception e) {
            throw e;
        }
        /*finally{
           close(); 
        }*/
    }
    
    
        
    // You need to close the resultSet
    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
    
    
}
